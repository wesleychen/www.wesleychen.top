# WesleyChen website develop

## Rules

1. 代码默认采用格式化的图片，便于查看和分享到其他平台


## UI Theme

1. 采用简洁UI风格
1. Blog内容用markdown编辑或MDX编辑


## 搜索引擎优化

1. SEO关键词优化
1. 默认支持Bing搜索


## Features

[] 静态生成网站发布
[] 支持前端代码可见html,js,css
[] 支持Bing搜索引擎搜索
[] 支持Markdown编辑Blog
[] webhook自动发布