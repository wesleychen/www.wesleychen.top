import Head from "next/head";

interface MetaProps {
  title: string;
  keywords: string;
  description: string;
  image: string;
}

export default function Meta({
  title,
  keywords,
  description,
  image,
}: MetaProps) {
  return (
    <Head>
      <title>{title}</title>
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <meta name="og:title" content={title} />
      <meta name="og:description" content={description} />
      {image && <meta name="og:image" content={image} />}
    </Head>
  );
}
