import { useCallback, useState } from "react";
import classnames from "classnames";
import Image from "next/image";
import Link from "next/link";
import styles from "./layout.module.scss";

export function Header() {
  const [menu, setMenu] = useState(false);
  const toggle = useCallback(() => {
    setMenu((prevMenu) => !prevMenu);
  }, []);

  return (
    <header className={styles.header}>
      <div className={styles.logo} title="首页">
        <Link href="/" passHref>
          <a>
            <Image
              src="/logo.png"
              width="100"
              height="100"
              alt="wesleychen.top website logo"
            />
          </a>
        </Link>
      </div>
      <div className={styles.navbar}>
        <button
          className={classnames(styles.menu, {
            [styles.active]: menu,
          })}
          onClick={toggle}
        />
        <nav>
          <ul className={classnames({ [styles.active]: menu })}>
            <li>
              <Link href="/software">软件工程</Link>
            </li>
            <li>
              <Link href="/test">代码测试</Link>
            </li>
            <li>
              <Link href="/open-source">开源</Link>
            </li>
            <li>
              <Link href="/book-list">书单</Link>
            </li>
            <li>
              <Link href="/tool-list">工具推荐</Link>
            </li>
            <li>
              <Link href="/about">关于网站</Link>
            </li>
          </ul>
        </nav>
      </div>
      {/* TODO: add Bing search function */}
    </header>
  );
}
export default Header;
