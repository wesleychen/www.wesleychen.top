import React from "react";
import styles from "./layout.module.scss";
import Footer from "./footer";
import Header from "./header";

export const Layout: React.FC<{ children: React.ReactNode }> = function (
  props
) {
  return (
    <div className="website">
      <Header />
      <main className={styles.container}>
        <div className={styles.main}>{props.children}</div>
      </main>
      <Footer />
    </div>
  );
};

export default Layout;
