import Image from "next/image";
import styles from "./layout.module.scss";

export function Footer() {
  return (
    <footer className={styles.footer}>
      {/* TODO: add about website */}
      <a href="#">关于网站</a>
      &nbsp; &copy;2022-现在 卫斯理.陈 版权所有 备案:
      <br />
      Power By: &nbsp;
      <a href="https://nextjs.org/" rel="noreferrer" target="_blank">
        Vercel Next.js
      </a>
      <br />
      <a
        href="https://gitee.com/wesleychen/www.wesleychen.top"
        className={styles.gitee}
        rel="noreferrer"
        target="_blank"
      >
        <Image src="/gitee.svg" alt="gitee" layout="fill" />
      </a>
    </footer>
  );
}

export default Footer;
