import type { NextApiRequest, NextApiResponse } from "next";

interface ListItem {
  text: string;
  items: { title: string; path: string }[];
}

type Data = {
  categories: ListItem[];
  tags: ListItem[];
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({ categories: [], tags: [] });
}
