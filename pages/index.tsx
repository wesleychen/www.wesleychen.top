import type { NextPage } from "next";
import { useState } from "react";
import Link from "next/link";
import styles from "./index.module.scss";

interface RecommendedItem {
  title: string;
  description: string;
  url: string;
  image?: string;
}

const Home: NextPage = () => {
  const [items] = useState<RecommendedItem[]>([
    {
      title: "软件工程师的“我做好了”",
      description: "在软件开发过程中如何定义“我做好了”",
      url: "/blog/123",
      image:
        "https://pic1.zhimg.com/v2-0e124da75ecb8cf3bed8d3414fa34307_r.jpg?source=172ae18b",
    },
    {
      title: "如何实现授权与认证",
      description:
        "几乎每个软件产品都会遇到用户的授权认证过程，实际开发过程中，我们是如何保证安全的情况下，较好的实现授权认证过程",
      url: "/blog/45",
    },
  ]);
  const [tags] = useState<string[]>([
    "React",
    "Vue",
    "Deno",
    "CSS",
    "HTML",
    "JavaScript",
    "Jest",
    "Golang",
  ]);

  return (
    <div className={styles.wrapper}>
      <section className={styles.recommend}>
        <ul>
          {items.map((item) => {
            return (
              <li key={item.url}>
                <h3>
                  <Link href={item.url}>{item.title}</Link>
                </h3>
                <p>{item.description}</p>
              </li>
            );
          })}
        </ul>
      </section>
      <section className={styles.tags}>
        <h2>Tags</h2>
        <div className={styles.tagsWrapper}>
          {tags.map((tag) => (
            <a key={tag} href="/tags?">
              #{tag}
            </a>
          ))}
        </div>
      </section>
    </div>
  );
};

export default Home;
