# exec directory in workspace
# Not has CI/CD pipeline, use shell replace

# Lint
yarn run lint

# Test

# Build
yarn run build

# Deploy
rsync -r .next Tencent1:/usr/share/nginx/www.wesleychen.top
rsync package.json Tencent1:/usr/share/nginx/www.wesleychen.top
rsync yarn.lock Tencent1:/usr/share/nginx/www.wesleychen.top

# Restart service
echo "Go to remote instance, and restart service. pm2 restart wesleychen.top"
